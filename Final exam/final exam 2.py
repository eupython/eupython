a = input()

even = 0
odd = 0

for i in a:
    if int(i) % 2 == 0:
        even += 1
    else:
        odd += 1

print("Even: %d, odd: %d" % (even, odd))

from random import randint

b = [randint(0, 50) for i in range(10)]
print(b)
even = odd = even_count = 0

for i in b:
    if i % 2:
        odd += i
    else:
        even += i
        even_count += 1

print(f'Четные: {even / even_count}\n Нечетные: {odd / (len(b) - even_count)}')