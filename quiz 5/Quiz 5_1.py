rom
random
import randint

b = [randint(0, 50) for i in range(10)]
print(b)
even = odd = even_count = 0

for i in b:
    if i % 2:
        odd += i
    else:
        even += i
        even_count += 1

print(f'Even: {even / even_count}\n Odd: {odd / (len(b) - even_count)}')
